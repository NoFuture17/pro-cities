$(document).on('ready', function() {
    // Скрытие и появление описания новости
    var news_preview = $('.b-main-news-preview');
    if (news_preview.length > 0) {
        $(document).on('mouseenter', '.b-main-news-preview .b-main-news-preview-item', function (e) {
            $(this).find('.b-main-news-preview-info-block__wrapper').slideDown(200);
        });
        $(document).on('mouseleave', '.b-main-news-preview .b-main-news-preview-item', function (e) {
            $(this).find('.b-main-news-preview-info-block__wrapper').slideUp(200);
        });
    }
});